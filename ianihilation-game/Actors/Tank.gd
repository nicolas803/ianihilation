extends KinematicBody2D

signal health_changed(health)
signal ammu_counter_changed(bullet_stock)

export (int) var speed = 200
export (float) var rotation_speed = 1.5
export (int) var max_health = 100
export (int) var max_bullet_stock = 5

var velocity = Vector2()
var rotation_dir = 0
var health = 1
var bullet_stock = 0

onready var BULLET = preload("bullet.tscn")

func _ready():
	health = max_health
	bullet_stock = max_bullet_stock
	emit_signal("health_changed", health)
	emit_signal("ammu_counter_changed", bullet_stock)

func get_input():
	rotation_dir = 0
	velocity = Vector2()
	if Input.is_action_pressed('ui_right'):
		rotation_dir += 1
	if Input.is_action_pressed('ui_left'):
		rotation_dir -= 1
	if Input.is_action_pressed('ui_down'):
		velocity = Vector2(-speed, 0).rotated(rotation)
	if Input.is_action_pressed('ui_up'):
		velocity = Vector2(speed, 0).rotated(rotation)
	if Input.is_action_just_pressed("ui_select") && bullet_stock:
		self.shoot(BULLET)

func _physics_process(delta):
	get_input()
	rotation += rotation_dir * rotation_speed * delta
	velocity = move_and_slide(velocity)
	if health < 1:
		self.die()

func hit(degat):
	health -= degat
	health = max(0, health)
	emit_signal("health_changed", health)

func shoot(bullet_type):
	var bullet =  bullet_type.instance()
	get_node("/root/WorldEnvironment").add_child(bullet)
	bullet.global_position = $shot_spawn.global_position
	bullet.velocity = Vector2(1, 0).rotated(rotation)
	bullet.shooter = self
	bullet_stock -= 1
	emit_signal("ammu_counter_changed", bullet_stock)
	
func reload():
	bullet_stock += 1
	emit_signal("ammu_counter_changed", bullet_stock)

func die():
	self.queue_free()
