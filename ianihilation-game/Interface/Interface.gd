extends Control

signal health_changed(health)
signal ammu_counter_changed(bullet_stock)

func _on_Blue_Tank_health_changed(health):
	emit_signal("health_changed", health)

func _on_Blue_Tank_ammu_counter_changed(bullet_stock):
	emit_signal("ammu_counter_changed", bullet_stock)
